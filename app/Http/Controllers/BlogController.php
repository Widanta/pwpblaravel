<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Blog;
use App\Models\Category;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $blogs = Blog::all();
        return view('blogs.index', compact('blogs'));
    }

    public function create()
    {
        $blogs = Blog::all();
        $categories = Category::all();
        return view('blogs.create', compact('blogs','categories'));
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'title' => 'min:1|required|max:255',
            'slug' => 'required',
            'content' => 'required',
            'user_id' => 'required',
            'category_id' => 'required'

        ]);


        $blogs = Blog::create($validate);
        return redirect()->route('index');
    }

    public function show($id)
    {
        $blogs = Blog::find($id);
        return view('blogs.show', compact('blogs'));
    }

    public function edit($id)
    {
        $blogs = Blog::find($id);
        $categories = Category::all();
        return view('blogs.edit', compact('blogs','categories'));
    }

    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'title' => 'required|max:255|',
            'slug' => 'required',
            'content' => 'required',
            'user_id' => 'required',
            'category_id' => 'required'
        ]);

        $blogs = Blog::find($id);
        $blogs->update($validate);
        return redirect()->route('index');
    }

    public function destroy($id)
    {
        $blogs = Blog::find($id);
        $blogs->delete();
        return redirect()->route('index');
    }
}
