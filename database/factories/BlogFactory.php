<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class BlogFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(mt_rand(2, 8)),
            'slug' => $this->faker->slug,
            'content' => collect($this->faker->paragraph(mt_rand(10, 20)))->map(fn ($p) => "<p>{$p}</p>")->implode(''),
            'user_id' => 1,
            'category_id' => mt_rand(1, 3),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
    }
}
