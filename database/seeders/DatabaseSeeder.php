<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Blog;
use App\Models\Category;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => null,
            'password' => bcrypt('password'),
            'remember_token' => null,
            'created_at' => date('Y-m-d'),
            'updated_at'  => date('Y-m-d'),

        ]);
        Category::factory(3)->create();
        Blog::factory(10)->create();
    }
}
