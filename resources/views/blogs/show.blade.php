@extends('layouts.template')

@include('layouts.navbar')
@section('content')

<div class="container">
    <div class="row justyfy-content-center">
        <a href="{{ route('home') }}" class="mb-2 text-decoration-none"> <i class="fa fa-chevron-left" aria-hidden="true"></i> Kembali</a>
        <div class="col">
            <p>Diperbaharui : {{ $blogs->created_at->format('d M Y') }} By <span class="text-primary fw-bold">{{ $blogs->user->name }}</span></p>
            <img src="https://placekitten.com/1000/500" class="img-fluid d-block m-auto mb-2" alt="">
            <h1>{{ $blogs->title }}</h1>
            <p class="m-0 my-3">Category : <span class="text-primary fw-bold">{{ $blogs->category->name }}</span></p>
            {!! $blogs->content !!}
        </div>
    </div>
</div>
@endsection