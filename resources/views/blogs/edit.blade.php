@extends('layouts.template')

@include('layouts.navbar')
@section('content')

<div class="container">
    <form action="{{ route('update', $blogs->id) }}" method="post">
        @method('put')
        @csrf
        <div class="mb-3">
          <label for="title" class="form-label">Judul <span class="text-danger fw-bold">*</span></label>
          <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title',$blogs->title) }}" >
          @error('title')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="mb-3">
          <label for="slug" class="form-label">Slug <span class="text-danger fw-bold">*</span></label>
          <input type="text" class="form-control" id="slug" name="slug" value="{{ $blogs->slug }}">
        </div>
        <div class="mb-3">
          <label for="" class="form-label">Category <span class="text-danger fw-bold">*</span></label>
          <select name="category_id" id="category" class="form-select @error('category') is-invalid @enderror">
            <option value="{{ $blogs->category->id }}">Category : {{ $blogs->category->name }}</option>
            @foreach ($categories as $category)
              <option  value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
          </select>
          @error('category')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="mb-3">
          <label for="content" class="form-label">Content <span class="text-danger fw-bold">*</span></label>
          <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content" rows="3">{{ old('content',$blogs->content) }}</textarea>
          @error('content')
            <div class="invalid-feedback">
              {{ $message }}
            </div>

          @enderror
        </div>
        <div class="mb-3">
          <label for="user_id" class="form-label">Author <span class="text-danger fw-bold">*</span></label>
          <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
          <input type="text" class="form-control"  value="{{ Auth::user()->name }}" disabled readonly >
        </div>
        <button type="submit" class="btn btn-primary">kirim</button>
    </form>
</div>

@endsection