<?php
$i = 1;
?>
@extends('layouts.template')

@include('layouts.navbar')
@section('content')
<div class="container">
  <div class="table-responsive">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th scope="col">
            <a href="{{ route('create') }}" class="text-decoration-none"><p class="fw-bold m-0"><i class="fa fa-plus" aria-hidden="true"></i></p></a>
          </th>
          <th scope="col">Title</th>
          <th scope="col">Category</th>
          <th scope="col">Slug</th>
          <th scope="col">Content</th>
          <th scope="col">Author</th>
          <th scope="col">aksi</th>
        </tr>
      </thead>
      <tbody>
      @foreach($blogs as $row) 
        <tr>
          <th><?= $i++; ?></th>
          <td>{{ $row->title }}</td>
          <td>{{ $row->category->name }}</td>
          <td>{{ $row->slug }}</td>
          <td>{{ $row->content }}</td>
          <td>{{ $row->user->name }}</td>
          <td>
            <a href="{{ route('show', $row->id) }}" class="btn btn-outline-success"><i class="fa fa-eye" aria-hidden="true"></i></a>
            <a href="{{ route('edit', $row->id) }}" class="btn btn-outline-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>

            {{-- yang ini ya ges --}}
            <form action="{{ route('destroy', $row->id) }}" method="post">
              @method('delete')
              @csrf
            <button type="submit" class="btn btn-outline-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </form>
    
    
          </td>
        </tr>
        @endforeach
    
      </tbody>
    </table>
  </div>
    
</div>

@endsection