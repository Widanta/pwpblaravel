@extends('layouts.template')

@include('layouts.navbar')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-9">
            <div class="row">
                @foreach ($blogs as $blog) 
                <div class="col-xl-4 col-lg-4 col-md-6 col-12 mb-4">
                    <a class="text-decoration-none text-dark" href="{{ route('show' , $blog->id) }}">
                        <div class="card border-0 shadow rounded-3">
                            <img src="https://placekitten.com/500/300" class="card-img-top rounded-top-3" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bold text-capitalize mt-3">{{ $blog->title}}</h5>
                                <p class="card-text m-0 my-3">Category : <span class="text-primary fw-bold">{{ $blog->category->name }}</span></p>
                                {!! Str::limit($blog->content, 150) !!}
                                <p class="card-text text-muted mt-1">{{ $blog->created_at->diffForHumans()}}...</p>
                            </div>
                          </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-xl-3">

            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Cari..">
                <button class="btn btn-secondary" type="submit" id="button-addon2">Cari</button>
            </div>
            <div class="card bg-transparent border-secondary">
                <div class="card-body">
                    <h3 class="text-secondary fw-bold ms-2 fst-italic">Category</h3>
                    <ul class="list-group">
                        @foreach ($categories as $category)
                        <li class="list-group-item bg-transparent border-0">{{ $category->name }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection