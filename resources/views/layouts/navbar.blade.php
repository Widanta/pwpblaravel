<nav class="navbar navbar-expand-lg bg-secondary navbar-dark mb-5">
    <div class="container">
      <a class="navbar-brand" href="#">BikinBlog</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-auto">
          {{-- belum login --}}
          @guest
          <li class="nav-item fs-4 mx-1">
            <a class="nav-link" href="/"><i class="fa fa-home" aria-hidden="true"></i></a>
          </li>
          @if (Route::has('login'))
          <li class="nav-item ">
            <a class="nav-link fs-4 mx-1" href="{{ route('login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i></a>
          </li>
          @endif
          {{-- sudah login --}}
          @else
          <li class="nav-item fs-4 mx-1">
            <a class="nav-link" href="/home"><i class="fa fa-home" aria-hidden="true"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link fs-4 mx-1" href="/blogs"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a>
          </li>
          <li class="nav-item mx-1 mt-2">
              <div class="dropdown">
                  <a href="#" class="text-decoration-none text-light" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user-circle-o fs-4 mx-2" aria-hidden="true"></i> <span>{{ Auth::user()->name }}</span>
                  </a>
                  <ul class="dropdown-menu text-center">
                      <a class="dropdown-item" href=""><i class="fa fa-user mx-1 text-secondary" aria-hidden="true"></i> Profile</a> 
                      
                      <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out mx-1 text-secondary" aria-hidden="true"></i> Logout
                      </a>
      
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                          @csrf
                      </form>
                  </ul>
              </div>
          </li>
          @endguest
        </ul>
      </div>
    </div>
  </nav>