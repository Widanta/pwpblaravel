</body>

<footer id="footer">
    
    <section id="footer-top">
        <div class="container">
            <div class="row justify-content-between pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-12 my-auto">
                    <h1><i class="fa fa-newspaper-o" aria-hidden="true"></i> BikinBlog</h1>
                    <p>BikinBlog adalah sebuah platform online untuk membuat blog. Sharing informasi untuk berbagai hal.</p>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-6 my-auto">
                    <a href="{{ route('home') }}" class="text-decoration-none text-light">
                        <p><i class="fa fa-circle me-2 text-secondary" aria-hidden="true"></i> Home</p>
                    </a>
                    <a href="{{ route('index') }}" class="text-decoration-none text-light">
                        <p><i class="fa fa-circle me-2 text-secondary" aria-hidden="true"></i> Blog</p>
                    </a>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-6 my-auto">
                    <a href="{{ route('create') }}" class="text-decoration-none text-light">
                        <p><i class="fa fa-circle me-2 text-secondary" aria-hidden="true"></i> Create</p>
                    </a>
                    <a href="#" class="text-decoration-none text-light">
                        <p><i class="fa fa-circle me-2 text-secondary" aria-hidden="true"></i> Update</p>
                    </a>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-6 my-auto">
                    <a href="{{ route('login') }}" class="text-decoration-none text-light">
                        <p><i class="fa fa-circle me-2 text-secondary" aria-hidden="true"></i> Login</p>
                    </a>
                    <a href="{{ route('register') }}" class="text-decoration-none text-light">
                        <p><i class="fa fa-circle me-2 text-secondary" aria-hidden="true"></i> Register</p>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <hr>
    <section id="footer-bottom">
        <div class="container">
            <div class="row text-center">
                <div class="col">
                    <p>© 2022 BikinBlog | All Rights Reserved</p>
                </div>
            </div>
        </div>
    </section>
</footer>

<script src="{{ asset('js/app.js') }}"></script>
</html>