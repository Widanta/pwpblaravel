<?php

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Route;
use App\Models\Blog;
use App\Models\Category;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $blogs = Blog::all();
    $categories = Category::all();
    return view('index', compact('blogs', 'categories'));
})->middleware('guest');

// Route::resource('blogs','BlogController');

Route::middleware(['auth'])->group(function () {
    Route::get('/blogs', [BlogController::class, 'index'])->name('index');
    Route::get('/blogs/create', [BlogController::class, 'create'])->name('create');
    Route::post('/', [BlogController::class, 'store'])->name('store');
    Route::get('/blogs/{id}/edit', [BlogController::class, 'edit'])->name('edit');
    Route::put('/blogs/{id}', [BlogController::class, 'update'])->name('update');
    Route::get('/blogs/{id}', [BlogController::class, 'show'])->name('show');
    Route::delete('/blogs/{id}', [BlogController::class, 'destroy'])->name('destroy');
});


Auth::routes();


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
